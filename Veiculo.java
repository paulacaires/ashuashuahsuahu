package com.mycompany.g6;

public class Veiculo {
    private String placa;
    private String modelo;
    private String marca;
    private String cor;
    private int ano;
    private String estado;

    public Veiculo(String placa, String modelo, String marca, String cor, int ano, String estado) {
        this.placa = placa;
        this.modelo = modelo;
        this.marca = marca;
        this.cor = cor;
        this.ano = ano;
        this.estado = estado;
    }    

    public String getPlaca() {
        return placa;
    }

    public String getModelo() {
        return modelo;
    }

    public String getMarca() {
        return marca;
    }

    public String getCor() {
        return cor;
    }

    public int getAno() {
        return ano;
    }

    public String getEstado() {
        return estado;
    } 
}
